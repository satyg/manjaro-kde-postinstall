set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths

### EXPORT ###
set fish_greeting
set TERM "xterm-256color"
set EDITOR "emacsclient -t -a ''"
set VISUAL "emacsclient -c -a emacs"
set -x MANPAGER '/bin/bash -c "vim -MRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" -c \"nmap q :qa<CR>\"</dev/tty <(col -b)"'
export PATH="$HOME/.cargo/bin:$PATH"
### END OF EXPORT ###

### COLORS ###
set fish_color_normal '#8be9fd'
set fish_color_autosuggestion '#6272a4'
set fish_color_command '#8be9fd'
set fish_color_error '#ff5555'
set fish_color_param '#8be9fd'
### END OF COLORS ###

### ALIASES ###
# navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'

# Using "exa" instead of "ls"
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias l.='exa -a | egrep "^\."'

# grep coloring
alias grep='grep --color=auto'

# Ask for confirmation before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# emacs
alias emacs="emacsclient -c -a 'emacs'"
alias doomsync="~/.emacs.d/bin/doom sync"
alias doomdoctor="~/.emacs.d/bin/doom doctor"
alias doomupgrade="~/.emacs.d/bin/doom upgrade"
alias doompurge="~/.emacs.d/bin/doom purge"
### END OF ALIASES ###

### FUNCTIONS ###
function sudo --description "Replacement for Bash 'sudo !!' command to run last command using sudo."
    if test "$argv" = !!
    eval command sudo $history[1]
else
    command sudo $argv
    end
end
### END OF FUNCTIONS ###

### PERSONAL ALIASES ###
alias rapport='python3 ~/script/python/LaTeX/rapport'
alias pdfmerge='python3 ~/script/python/pdf-merge/pdfmerge'
alias risk='code ~/MEGA/Skola/RISK/'
alias hu='code ~/MEGA/Skola/HÅLLBAR/'
alias script='code ~/MEGA/scripts'
