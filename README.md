# Post installation of Manjaro KDE
This script will setup a predefined configuration of KDE for Manjaro. The software inluded in this script is defined by my personal liking and my own workflow. 

---
## Software to be installed
The software that will be installed on the system comes both from pacman, AUR, and github. 

### Pacman
- [vim](https://wiki.archlinux.org/title/vim)
- [git](https://wiki.archlinux.org/title/git)
- [discord](https://wiki.archlinux.org/title/Discord)
- [telegram-desktop](https://wiki.archlinux.org/title/Telegram)
- [code](https://wiki.archlinux.org/title/Visual_Studio_Code)
- [fish](https://wiki.archlinux.org/title/Fish)
- [texlive-bin](https://wiki.archlinux.org/title/TeX_Live)
- [texlive-formatsextra](https://wiki.archlinux.org/title/TeX_Live)
- [texlive-latexextra](https://wiki.archlinux.org/title/TeX_Live)
- [texlive-bibtexextra](https://wiki.archlinux.org/title/TeX_Live)
- [texstudio](https://en.wikipedia.org/wiki/TeXstudio)
- [exa](https://archlinux.org/packages/community/x86_64/exa/)
- [ttf-jetbrains-mono](https://archlinux.org/packages/community/any/ttf-jetbrains-mono/)
- [adobe-source-code-pro-fonts](https://archlinux.org/packages/extra/any/adobe-source-code-pro-fonts/)
- [awesome-terminal-fonts](https://archlinux.org/packages/community/any/awesome-terminal-fonts/)
- [ttf-ubuntu-font-family](https://archlinux.org/packages/community/any/ttf-ubuntu-font-family/)
- [alacritty](https://wiki.archlinux.org/title/Alacritty)
- [thunderbird](https://wiki.archlinux.org/title/thunderbird)
- [unzip](https://archlinux.org/packages/extra/x86_64/unzip/)
- [libreoffice-still](https://archlinux.org/packages/extra/x86_64/libreoffice-still/)
- [make](https://archlinux.org/packages/core/x86_64/make/)

### AUR

- [nerd-fonts-mononoki](https://aur.archlinux.org/packages/nerd-fonts-mononoki)
- [openrgb](https://aur.archlinux.org/packages/openrgb)
- [davmail](https://aur.archlinux.org/packages/davmail)

### Git

- [AUR-helper](https://aur.archlinux.org/yay.git)
- [RTL8821cu](https://github.com/brektrou/rtl8821CU.git) (Optional wi-fi driver)

---
## Changes to the system after software has been installed

After the software has been installed, a few changes will be done to the config files and some new config files will be added.

The changes is in `/etc/pacman.conf` where the line that says `#Color` till be uncomment in order to display more colors when using the `pacman` command, for more readability. 

Since the system will run `fish` as user-shell, you will be prompted to enter password when the change occurs. Both the change to fish-shell and the change in pacman config file will be done in the function called `postInstallation` as can be seen below.

```python
def postInstall():
    commands = ['sed \'s/#Color/Color/\' /etc/pacman.conf',
                'chsh -s /usr/bin/fish']
    
    for command in commands:
        try:
            os.system(command)
        except:
            print(f'{red}Unable to run the command {command}... Skipping{default}')
```

Other that mentioned changes, a few new config files will be added to the system. Those will effect three different sources within the system; **fish-shell**, **Alacritty**, and **OpenRGB**. Adding those files is performed in the function called `dotfiles` as can be seen below.

```python
def dotfiles():
    commands = ['mv ~/manjaro-kde/dotfiles/config_fish ~/.config/fish/',
                'mv ~/manjaro-kde/dotfiles/fish_variables ~/.config/fish/',
                'mv ~/manjaro-kde/dotfiles/satyg03.orp ~/.config/OpenRGB/',
                'mv ~/manjaro-kde/dotfiles/alacritty.yml ~/.config/alacritty/']
    
    for command in commands:
        try:
            os.system(command)
        except:
            print(f'{red}Could not execute the command {command}...{default}')
```

---

## Optional parts of the script
Once the mandatory parts of the script is done, you will be prompted with a question wheter to install wifi drivers or not. Those drivers is hard coded to be `rtl8821cu` and will be needed to be changed inside the code - if that is your wish. However, when answering yes to that question (default is set to no) you will get mentioned drivers installed. The function handeling the installation of wifi-drivers is called `wifi` and looks like below.

```python
def wifi():
    os.mkdir(os.path.expanduser('~/rtl8821cu'))
    os.chdir(os.path.expanduser('~/rtl8821cu'))
    commands = ['yay -S rtl8821cu-git --noconfirm',
                'make', 'sudo make install']

    for command in commands:
        try:
            os.system(command)
        except:
            print(f'{red}Unable to make install{default}')
```

***NOTE***: *The question to answer about this installation is done in the `main` function* 